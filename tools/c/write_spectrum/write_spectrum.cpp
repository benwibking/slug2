/*********************************************************************
Copyright (C) 2014 Robert da Silva, Michele Fumagalli, Mark Krumholz
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

////////////////////////////////////////////////////////////////////////
// This is a utility program that uses slug's atmosphere classes to
// print out the spectrum of a single star
////////////////////////////////////////////////////////////////////////

#ifdef __INTEL_COMPILER
// Need this to fix a bug in the intel compilers relating to c++11
namespace std
{
  typedef decltype(nullptr) nullptr_t;
}
#endif

#include <cstdio>
#include <cmath>
#include <iomanip>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include "../../../src/slug.H"
#include "../../../src/slug_IO.H"
#include "../../../src/tracks/slug_tracks_mist.H"
#include "../../../src/specsyn/slug_specsyn.H"
#include "../../../src/specsyn/slug_specsyn_hillier.H"
#include "../../../src/specsyn/slug_specsyn_kurucz.H"
#include "../../../src/specsyn/slug_specsyn_pauldrach.H"
#include "../../../src/specsyn/slug_specsyn_planck.H"
#include "../../../src/specsyn/slug_specsyn_sb99.H"
#include "../../../src/specsyn/slug_specsyn_sb99hruv.H"

////////////////////////////////////////////////////////////////////////
// Usage message
////////////////////////////////////////////////////////////////////////

void print_usage() {
  using namespace std;
  cerr << "usage: write_spectrum [-mode MODE] [-WR WR] mass lum Teff"
       << endl;
  cerr << endl;
  cerr << "Utility to write out a stellar spectrume" << endl;
  cerr << endl;
  cerr << "positional arguments:" << endl;
  cerr << "  mass                stellar mass in Msun" << endl;
  cerr << "  lum                 stellar lumnosity in Lsun" << endl;
  cerr << "  Teff                stellar effective temperature in K" << endl;
  cerr << endl;
  cerr << "optional arguments:" << endl;
  cerr << "  -h, --help          show this help message and exit" << endl;
  cerr << "  -mode MODE          spectral synthesis mode; valid options are:"
       << endl;
  cerr << "                      planck, kurucz, kurucz+hillier," << endl;
  cerr << "                      kurucz+pauldrach, sb99; default is sb99"
       << endl;
  cerr << "  -Z Z                metallicity relative to solar (default = 1)"
       << endl;
  cerr << "  -WR WRtype          type of WR atmosphere; valid options are:"
       << endl;
  cerr << "                      WNL, WNE, WC69, WC45, WO, None (not a WR star); default"
       << " is None" << endl;
}


////////////////////////////////////////////////////////////////////////
// Input parser
////////////////////////////////////////////////////////////////////////

void parse_args(int argc, char *argv[],
		double &m, double &l, double &Teff, double &Z,
		specsynMode& specsyn_mode, WRtype& WR) {
  using boost::lexical_cast;
  using boost::bad_lexical_cast;

  // Set defaults
  specsyn_mode = SB99;
  WR = NONE;
  Z = 1.0;

  // Loop through input arguments
  int argptr = 1;
  bool read_m = false;
  bool read_l = false;
  bool read_Teff = false;
  while (argptr < argc) {

    // Is this a positional argument or an optional one?
    const char *arg = argv[argptr];
    if (arg[0] == '-') {

      // Optional argument case

      // Bail if given just - as an argument
      if (strlen(arg) == 1) {
	std::cerr << "error: could not parse -" << std::endl;
	print_usage();
	exit(1);
      }

      // Check flag
      if (!strcmp(arg, "-h") || !strcmp(arg, "--help")) {

	// Help flag
	print_usage();
	exit(1);
	
      } else if (!strcmp(arg, "-mode")) {

	// Mode flag
	argptr++;
	if (argptr == argc) {
	  std::cerr << "error: expected specsyn mode after -mode"
		    << std::endl;
	  print_usage();
	  exit(1);
	}
	if (!strcmp(argv[argptr], "planck")) {
	  specsyn_mode = PLANCK;
	} else if (!strcmp(argv[argptr], "kurucz")) {
	  specsyn_mode = KURUCZ;
	} else if (!strcmp(argv[argptr], "kurucz+hillier")) {
	  specsyn_mode = KURUCZ_HILLIER;
	} else if (!strcmp(argv[argptr], "kurucz+pauldrach")) {
	  specsyn_mode = KURUCZ_PAULDRACH;
	} else if (!strcmp(argv[argptr], "sb99_hruv")) {
	  specsyn_mode = SB99_HRUV;
	} else if (!strcmp(argv[argptr], "sb99")) {
	  specsyn_mode = SB99;
	} else {
	  std::cerr << "error: invalid specsyn_mode "
		    << argv[argptr]
		    << std::endl;
	  print_usage();
	  exit(1);
	}

      } else if (!(strcmp(arg, "-Z"))) {

        // Metqllicity flag
        argptr++;
        if (argptr == argc) {
          std::cerr << "error: expected metallicity value after -Z"
                    << std::endl;
          print_usage();
          exit(1);
        }

        // Read metallicity
        try {
          Z = lexical_cast<double>(argv[argptr]);
        } catch (const bad_lexical_cast& ia) {
          std::cerr << "error: expected numerical value for metallicity" 
                    << std::endl;
          print_usage();
          exit(1);
        }
        if (Z <= 0.0) {
          std::cerr << "error: expected positive value of metallicity"
                    << std::endl;
          print_usage();
          exit(1);
        }

      } else if (!(strcmp(arg, "-WR"))) {

	// WR flag
	argptr++;
	if (argptr == argc) {
	  std::cerr << "error: expected WR type after -WR"
		    << std::endl;
	  print_usage();
	  exit(1);
	}
	if (!strcmp(argv[argptr], "WC69")) {
	  WR = WC69;
	} else if (!strcmp(argv[argptr], "WC45")) {
	  WR = WC45;
	} else if (!strcmp(argv[argptr], "WNE")) {
	  WR = WNE;
	} else if (!strcmp(argv[argptr], "WNL")) {
	  WR = WNL;
	} else if (!strcmp(argv[argptr], "WO")) {
	  WR = WO;
	} else if (!strcmp(argv[argptr], "None")) {
	  WR = NONE;
	} else {
	  std::cerr << "error: invalid WR type "
		    << argv[argptr]
		    << std::endl;
	  print_usage();
	  exit(1);
	}
      } else {

	// Unrecognized argument
	std::cerr << "error: unrecognized option " << arg 
		  << std::endl;
	print_usage();
	exit(1);
      }

    } else {

      // Positional argument
      if (!read_m) {
	try {
	  m = lexical_cast<double>(argv[argptr]);
	  read_m = true;
	} catch (const bad_lexical_cast& ia) {
	  std::cerr << "error: expected numerical value for mass" << std::endl;
	  print_usage();
	  exit(1);
	}
      } else if (!read_l) {
	try {
	  l = lexical_cast<double>(argv[argptr]);
	  read_l = true;
	} catch (const bad_lexical_cast& ia) {
	  std::cerr << "error: expected numerical value for lum" << std::endl;
	  print_usage();
	  exit(1);
	}
      } else if (!read_Teff) {
	try {
	  Teff = lexical_cast<double>(argv[argptr]);
	  read_Teff = true;
	} catch (const bad_lexical_cast& ia) {
	  std::cerr << "error: expected numerical value for Teff" << std::endl;
	  print_usage();
	  exit(1);
	}
	
      }  else {

	// If we've already read m, r, and l, and we're finding more
	// positional arguments, something is wrong
	std::cerr
	  << "error: found more positional arguments than expected"
	  << std::endl;
	print_usage();
	exit(1);
      }
    }

    // Increment argument pointer
    argptr++;
  }

  // Make sure we have all reaquired inputs, and that values are valid
  if (!read_m || !read_l || !read_Teff) {
    std::cerr << "error: must specify mass, lum, and Teff" << std::endl;
    print_usage();
    exit(1);
  }
  if (m <= 0.0 || l <= 0.0 || Teff <= 0.0) {
    std::cerr << "error: mass, lum, and Teff must be positive" << std::endl;
    print_usage();
    exit(1);
  }
}

////////////////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {

  // Parse the command line
  specsynMode specsyn_mode;
  WRtype WR;
  double m, l, Teff, Z;
  parse_args(argc, argv, m, l, Teff, Z, specsyn_mode, WR);

  // Set path to atmospheres and tracks
  char *slug_dir_ptr = getenv("SLUG_DIR");
  std::string slug_dir;
  if (slug_dir_ptr != NULL)
    slug_dir = slug_dir_ptr;
  if (slug_dir.length() == 0)
    slug_dir = boost::filesystem::current_path().string();
  boost::filesystem::path slug_path(slug_dir);
  boost::filesystem::path atm_path = slug_path / "lib" / "atmospheres";
  boost::filesystem::path track_path = slug_path / "lib" / "tracks";

  // Create a tracks objects; we won't actually use this, but we need
  // to provide it to the atmospheres object as a way of communicating
  // the metallicity
  slug_ostreams ostreams;
  slug_tracks *tracks = static_cast<slug_tracks *>
    (new slug_tracks_mist(MIST_2016_VVCRIT_40, Z, track_path.c_str(),
                         ostreams));

  // Build the specsyn object
  slug_specsyn *specsyn = nullptr;
  switch (specsyn_mode) {
  case PLANCK: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_planck(tracks, nullptr, nullptr, ostreams, 0.0));
    break;
  }
  case KURUCZ: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_kurucz(atm_path.c_str(), tracks, nullptr, nullptr,
                               ostreams, 0.0));
    break;
  }
  case KURUCZ_HILLIER: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_hillier(atm_path.c_str(), tracks, nullptr, nullptr,
                                ostreams, 0.0));
    break;
  }
  case KURUCZ_PAULDRACH: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_pauldrach(atm_path.c_str(), tracks, nullptr, 
                                  nullptr, ostreams, 0.0));
    break;
  }
  case SB99: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_sb99(atm_path.c_str(), tracks, nullptr, nullptr,
                             ostreams, 0.0));
    break;
  }
  case SB99_HRUV: {
    specsyn = static_cast<slug_specsyn *>
      (new slug_specsyn_sb99hruv(atm_path.c_str(), tracks, nullptr, nullptr,
				 ostreams, 0.0));
    break;
  }
  }

  // Construct stardata object to pass to synthesizer
  slug_stardata star;
  star.logM = log10(m);
  star.logL = log10(l);
  star.logTeff = log10(Teff);
  star.logR = 0.5*(star.logL+constants::logLsun) 
    - 0.5*log10(4.0*M_PI) 
    - 0.5*constants::logsigmaSB - 2.0*star.logTeff
    - constants::logRsun;
  star.logg = constants::logG + star.logM +
    constants::logMsun - 2.0*star.logR - 
    2.0*constants::logRsun;
  star.WR = WR;

  // Get synthesized spectrum
  std::vector<double> wl = specsyn->lambda();
  std::vector<double> spec = specsyn->get_spectrum(star);

  // Write to stdout
  std::cout << "       lambda" << "   " << "     L_lambda" << std::endl;
  std::cout << "        [Ang]" << "   " << "  [erg/s/Ang]" << std::endl;
  std::cout << "-------------" << "   " << "-------------" << std::endl;
  for (std::vector<double>::size_type i=0; i<wl.size(); i++) {
    std::cout << std::setw(13) << wl[i] << "   "
	      << std::setw(13) << spec[i] << std::endl;
  }

  // Free
  delete specsyn;
  delete tracks;
}
