
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Probability Distribution Functions &#8212; slug 2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Output Files and Format" href="output.html" />
    <link rel="prev" title="Parameter Specification" href="parameters.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="probability-distribution-functions">
<span id="sec-pdfs"></span><h1>Probability Distribution Functions<a class="headerlink" href="#probability-distribution-functions" title="Permalink to this headline">¶</a></h1>
<p>The SLUG code regards the IMF, the CMF, the CLF, the SFH, and the extinction <span class="math notranslate nohighlight">\(A_V\)</span> as probability distribution functions – see <a class="reference internal" href="intro.html#ssec-slugpdfs"><span class="std std-ref">Probability Distribution Functions: the IMF, SFH, CMF, CLF, A_V distribution</span></a>. The code provides a generic file format through which PDFs can be specified. Examples can be found in the <code class="docutils literal notranslate"><span class="pre">lib/imf</span></code>, <code class="docutils literal notranslate"><span class="pre">lib/cmf</span></code>, <code class="docutils literal notranslate"><span class="pre">lib/clf</span></code>, and <code class="docutils literal notranslate"><span class="pre">lib/sfh</span></code> directories of the SLUG distribution.</p>
<p>PDFs in SLUG are generically written as functions</p>
<div class="math notranslate nohighlight">
\[\frac{dp}{dx} = n_1 f_1(x; x_{1,a}, x_{1,b}) + n_2 f_2(x; x_{2,a}, x_{2,b}) + n_3 f_3(x; x_{3,a}, x_{3,b}) + \cdots,\]</div>
<p>where <span class="math notranslate nohighlight">\(f_i(x; x_{i,a}, x_{i,b})\)</span> is non-zero only for <span class="math notranslate nohighlight">\(x \in [x_{i,a}, x_{i,b}]\)</span>. The functions <span class="math notranslate nohighlight">\(f_i\)</span> are simple continuous functional forms, which we refer to as <em>segments</em>. Functions in this form can be specified in SLUG in two ways.</p>
<section id="basic-mode">
<h2>Basic Mode<a class="headerlink" href="#basic-mode" title="Permalink to this headline">¶</a></h2>
<p>The most common way of specifying a PDF is in basic mode. Basic mode describes a PDF that has the properties that</p>
<ol class="arabic simple">
<li><p>the segments are contiguous with one another, i.e., <span class="math notranslate nohighlight">\(x_{i,b} = x_{i+1,a}\)</span></p></li>
<li><p><span class="math notranslate nohighlight">\(n_i f_i(x_{i,b}; x_{i,a}, x_{i,b}) = n_{i+1} f_{i+1}(x_{i+1,a}; x_{i+1,a}, x_{i+1,b})\)</span></p></li>
<li><p>the overall PDF is normalized such that <span class="math notranslate nohighlight">\(\int (dp/dx)\, dx = 1\)</span></p></li>
</ol>
<p>Given these constraints, the PDF can be specified fully simply by giving the <span class="math notranslate nohighlight">\(x\)</span> values that define the edges of the segments and the functional forms <span class="math notranslate nohighlight">\(f\)</span> of each segment; the normalizations can be computed from the constraint equations. Note that SFH PDFs cannot be described using basic mode, because they are not normalized to unity. Specifying a non-constant SFH requires advanced mode.</p>
<p>An example of a basic mode PDF file is as follows:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>###############################################################
# This is an IMF definition file for SLUG v2.
<span class="gh"># This file defines the Chabrier (2005) IMF</span>
<span class="gh">###############################################################</span>

# Breakpoints: mass values where the functional form changes
# The first and last breakpoint will define the minimum and
# maximum mass
breakpoints 0.08 1 120

# Definitions of segments between the breakpoints

# This segment is a lognormal with a mean of log_10 (0.2 Msun)
# and dispersion 0.55; the dispersion is in log base 10, not
# log base e
segment
type lognormal
mean 0.2
disp 0.55

# This segment is a powerlaw of slope -2.35
segment
type powerlaw
slope -2.35
</pre></div>
</div>
<p>This example represents a <a class="reference external" href="http://adsabs.harvard.edu/abs/2005ASSL..327...41C">Chabrier (2005)</a> IMF from <span class="math notranslate nohighlight">\(0.08 - 120\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span>, which is of the functional form</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{dp}{dm} \propto \left\{\begin{array}{ll} \exp[-\log(m/m_0)^2/(2\sigma^2)] (m/m_b)^{-1} , &amp; m &lt; m_b \\ \exp[-\log(m_b/m_0)^2/(2\sigma^2)] (m/m_b)^{-2.35}, &amp; m \geq m_b \end{array} \right.,\end{split}\]</div>
<p>where <span class="math notranslate nohighlight">\(m_0 = 0.2\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span>, <span class="math notranslate nohighlight">\(\sigma = 0.55\)</span>, and <span class="math notranslate nohighlight">\(m_b = 1\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span>.</p>
<p>Formally, the format of a basic mode file is as follows. Any line beginning with <code class="docutils literal notranslate"><span class="pre">#</span></code> is a comment and is ignored. The first non-empty, non-comment line in a basic mode PDF file must be of the form:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>breakpoints x1 x2 x3 ...
</pre></div>
</div>
<p>where <code class="docutils literal notranslate"><span class="pre">x1</span></code>, <code class="docutils literal notranslate"><span class="pre">x2</span></code>, <code class="docutils literal notranslate"><span class="pre">x3</span></code>, <code class="docutils literal notranslate"><span class="pre">...</span></code> are a non-decreasing series of real numbers. These represent the breakpoints that define the edges of the segment, in units of <span class="math notranslate nohighlight">\(M_\odot\)</span>. In the example given above, the breakpoints are are <span class="math notranslate nohighlight">\(0.08\)</span>, <span class="math notranslate nohighlight">\(1\)</span>, and <span class="math notranslate nohighlight">\(120\)</span>, indicating that the first segment goes from <span class="math notranslate nohighlight">\(0.08 - 1\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span>, and the second from <span class="math notranslate nohighlight">\(1 - 120\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span>.</p>
<p>After the <code class="docutils literal notranslate"><span class="pre">breakpoints</span></code> line, there must be a series of entries of the form:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>segment
type TYPE
key1 VAL1
<span class="gh">key2 VAL2</span>
<span class="gh">...</span>
</pre></div>
</div>
<p>where <code class="docutils literal notranslate"><span class="pre">TYPE</span></code> specifies what functional form describes the segment, and <code class="docutils literal notranslate"><span class="pre">key1</span> <span class="pre">VAL1</span></code>, <code class="docutils literal notranslate"><span class="pre">key2</span> <span class="pre">VAL2</span></code>, etc. are a series of (key, value) pairs the define the free parameters for that segment. In the example above, the first segment is described as having a <code class="docutils literal notranslate"><span class="pre">lognormal</span></code> functional form, and the keywords <code class="docutils literal notranslate"><span class="pre">mean</span></code> and <code class="docutils literal notranslate"><span class="pre">disp</span></code> specify that the lognormal has a mean of 0.2 <span class="math notranslate nohighlight">\(M_\odot\)</span> and a dispersion of 0.55 in <span class="math notranslate nohighlight">\(\log_{10}\)</span>. The second segment is of type <code class="docutils literal notranslate"><span class="pre">powerlaw</span></code>, and it has a slope of <span class="math notranslate nohighlight">\(-2.35\)</span>. The full list of allowed segment types and the keywords that must be specified with them are listed in the <a class="reference internal" href="#tab-segtypes"><span class="std std-ref">Segment Types</span></a> Table. Keywords and segment types are case-insensitive. Where more than one keyword is required, the order is arbitrary.</p>
<p>The total number of segments must be equal to one less than the number of breakpoints, so that each segment is described. Note that it is not necessary to specify a normalization for each segment, as the segments will be normalized relative to one another automatically so as to guarantee that the overall function is continuous.</p>
<span id="tab-segtypes"></span><table class="docutils align-default" id="id1">
<caption><span class="caption-text">Segment Types</span><a class="headerlink" href="#id1" title="Permalink to this table">¶</a></caption>
<colgroup>
<col style="width: 10%" />
<col style="width: 33%" />
<col style="width: 6%" />
<col style="width: 16%" />
<col style="width: 6%" />
<col style="width: 29%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Name</p></th>
<th class="head"><p>Functional form</p></th>
<th class="head"><p>Keyword</p></th>
<th class="head"><p>Meaning</p></th>
<th class="head"><p>Keyword</p></th>
<th class="head"><p>Meaning</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">delta</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(\delta(x-x_a)\)</span></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">exponential</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(\exp(-x/x_*)\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">scale</span></code></p></td>
<td><p>Scale length, <span class="math notranslate nohighlight">\(x_*\)</span></p></td>
<td></td>
<td></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">lognormal</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(x^{-1} \exp\{-[\log_{10}(x/x_0)]^2/2\sigma^2\}\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">mean</span></code></p></td>
<td><p>Mean, <span class="math notranslate nohighlight">\(x_0\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">disp</span></code></p></td>
<td><p>Dispersion in <span class="math notranslate nohighlight">\(\log_{10}\)</span>, <span class="math notranslate nohighlight">\(\sigma\)</span></p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">normal</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(\exp[-(x-x_0)^2/2\sigma^2]\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">mean</span></code></p></td>
<td><p>Mean, <span class="math notranslate nohighlight">\(x_0\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">disp</span></code></p></td>
<td><p>Dispersion, <span class="math notranslate nohighlight">\(\sigma\)</span></p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">powerlaw</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(x^p\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">slope</span></code></p></td>
<td><p>Slope, <span class="math notranslate nohighlight">\(p\)</span></p></td>
<td></td>
<td></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">schechter</span></code></p></td>
<td><p><span class="math notranslate nohighlight">\(x^p \exp(-x/x_*)\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">slope</span></code></p></td>
<td><p>Slope, <span class="math notranslate nohighlight">\(p\)</span></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">xstar</span></code></p></td>
<td><p>Cutoff, <span class="math notranslate nohighlight">\(x_*\)</span></p></td>
</tr>
</tbody>
</table>
</section>
<section id="variable-mode">
<h2>Variable Mode<a class="headerlink" href="#variable-mode" title="Permalink to this headline">¶</a></h2>
<p>Variable Mode works as an extension to Basic Mode. Instead of assigning a value to a parameter, you can define a PDF and then draw values for the parameter from it.</p>
<p>Formally, the format of a Variable Mode PDF file follows that of a Basic Mode PDF file, but with one addition. To specify a parameter as variable, the entry must be of the form:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>key1 V path/to/pdf.vpar
</pre></div>
</div>
<p>with the <code class="docutils literal notranslate"><span class="pre">V</span></code> instructing the code that the parameter is variable. The <code class="docutils literal notranslate"><span class="pre">.vpar</span></code> files are formatted as if they are standard Basic Mode PDF files. Variable Mode is an extension of Basic Mode, and it is not supported in Advanced Mode PDF files.</p>
<p>Any number of parameters belonging to a PDF can be made to vary, and the distributions from which their values are drawn can be constructed from any combination of the PDF segment types specified for Basic Mode.</p>
<p>An example of a Variable Mode PDF file for the IMF is as follows:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>###############################################################
# This is an IMF definition file for SLUG v2.
<span class="gh"># This file defines a power law PDF with variable slope</span>
<span class="gh">###############################################################</span>

# Breakpoints: mass values where the functional form changes
# The first and last breakpoint will define the minimum and
# maximum mass
breakpoints 0.08 120

# Definitions of segments between the breakpoints

# This segment is a powerlaw with slopes drawn from slope_pdf
segment
type powerlaw
slope V lib/imf/slope_pdf.vpar
</pre></div>
</div>
<p>An example of a parameter’s PDF file is as follows:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>###############################################################
# This is a parameter definition file for SLUG v2.
<span class="gh"># lib/imf/slope_pdf.vpar</span>
<span class="gh">###############################################################</span>

# Breakpoints
breakpoints -3.0 -1.0

# Draw parameters from a normal distribution
segment
type normal
mean -2.35
disp 0.1
</pre></div>
</div>
<p>The above examples correspond to a powerlaw IMF with a slope varying between -3.0 and -1.0, with the value drawn from a normal distribution.</p>
<p>While the Variable Mode implementation is very general, it is currently only enabled for the IMF. The new parameter values are drawn at the start of each galaxy or cluster realisation.</p>
</section>
<section id="advanced-mode">
<h2>Advanced Mode<a class="headerlink" href="#advanced-mode" title="Permalink to this headline">¶</a></h2>
<p>In advanced mode, one has complete freedom to set all the parameters describing the PDF: the endpoints of each segment <span class="math notranslate nohighlight">\(x_{i,a}\)</span> and <span class="math notranslate nohighlight">\(x_{i,b}\)</span>, the normalization of each segment <span class="math notranslate nohighlight">\(n_i\)</span>, and the functional forms of each segment <span class="math notranslate nohighlight">\(f_i\)</span>. This can be used to defined PDFs that are non-continuous, or that are overlapping; the latter option can be used to construct segments with nearly arbitrary functional forms, by constructing a Taylor series approximation to the desired functional form and then using a series of overlapping <code class="docutils literal notranslate"><span class="pre">powerlaw</span></code> segments to implement that series.</p>
<p>An example of an advanced mode PDF file is as follows:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>###############################################################
# This is a SFH definition file for SLUG v2.
# This defines a SF history consisting of a series of
# exponentially-decaying bursts with a period of 100 Myr and
# a decay timescale of 10 Myr, with an amplitude chosen to
<span class="gh"># give a mean SFR of 10^-3 Msun/yr.</span>
<span class="gh">###############################################################</span>

# Declare that this is an advanced mode file
advanced

# First exponential burst
segment
type exponential
min      0.0
max      1.0e8         # Go to 100 Myr
weight   1.0e5         # Form 10^5 Msun of stars over 100 Myr
scale    1.0e7         # Decay time 10 Myr

# Next 4 bursts
segment
type exponential
min      1.0e8
max      2.0e8
weight   1.0e5
scale    1.0e7

segment
type exponential
min      2.0e8
max      3.0e8
weight   1.0e5
scale    1.0e7

segment
type exponential
min      3.0e8
max      4.0e8
weight   1.0e5
scale    1.0e7

segment
type exponential
min      4.0e8
max      5.0e8
weight   1.0e5
scale    1.0e7
</pre></div>
</div>
<p>This represents a star formation history that is a series of exponential bursts, separated by 100 Myr, with decay times of 10 Myr. Formally, this SFH follows the functional form</p>
<div class="math notranslate nohighlight">
\[\dot{M}_* = n e^{-(t\,\mathrm{mod}\, P)/t_{\rm dec}},\]</div>
<p>where <span class="math notranslate nohighlight">\(P = 100\)</span> Myr is the period and <span class="math notranslate nohighlight">\(t_{\rm dec} = 10\)</span> Myr is the decay time, from times <span class="math notranslate nohighlight">\(0-500\)</span> Myr. The normalization constant <span class="math notranslate nohighlight">\(n\)</span> is set by the condition that <span class="math notranslate nohighlight">\((1/P) \int_0^P \dot{M}_* \,dt = 0.001\)</span> <span class="math notranslate nohighlight">\(M_\odot\;\mathrm{yr}^{-1}\)</span>, i.e., that the mean SFR averaged over a single burst period is 0.001 <span class="math notranslate nohighlight">\(M_\odot\;\mathrm{yr}^{-1}\)</span>.</p>
<p>Formally, the format of an advanced mode file is as follows. First, all advanced mode files must start with the line:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>advanced
</pre></div>
</div>
<p>to declare that the file is in advanced mode. After that, there must be a series of entries of the form:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>segment
type TYPE
min MIN
max MAX
weight WEIGHT
key1 VAL1
<span class="gh">key2 VAL2</span>
<span class="gh">...</span>
</pre></div>
</div>
<p>The <code class="docutils literal notranslate"><span class="pre">type</span></code> keyword is exactly the same as in basic mode, as are the segment-specific parameter keywords <code class="docutils literal notranslate"><span class="pre">key1</span></code>, <code class="docutils literal notranslate"><span class="pre">key2</span></code>, <span class="math notranslate nohighlight">\(\ldots\)</span>. The same functional forms, listed in the <a class="reference internal" href="#tab-segtypes"><span class="std std-ref">Segment Types</span></a> Table, are available as in basic mode. The additional keywords that must be supplied in advanced mode are <code class="docutils literal notranslate"><span class="pre">min</span></code>, <code class="docutils literal notranslate"><span class="pre">max</span></code>, and <code class="docutils literal notranslate"><span class="pre">weight</span></code>. The <code class="docutils literal notranslate"><span class="pre">min</span></code> and <code class="docutils literal notranslate"><span class="pre">max</span></code> keywords give the upper and lower limits <span class="math notranslate nohighlight">\(x_{i,a}\)</span> and <span class="math notranslate nohighlight">\(x_{i,b}\)</span> for the segment; the probability is zero outside these limits. The keyword <code class="docutils literal notranslate"><span class="pre">weight</span></code> specifies the integral under the segment, i.e., the weight <span class="math notranslate nohighlight">\(w_i\)</span> given for segment <span class="math notranslate nohighlight">\(i\)</span> is used to set the normalization <span class="math notranslate nohighlight">\(n_i\)</span> via the equation</p>
<div class="math notranslate nohighlight">
\[w_i = n_i \int_{x_{i,a}}^{x_{i,b}} f_i(x) \, dx.\]</div>
<p>In the case of a star formation history, as in the example above, the weight <span class="math notranslate nohighlight">\(w_i\)</span> of a segment is simply the total mass of stars formed in that segment. In the example given above, the first segment declaration sets up a PDF that with a minimum at 0 Myr, a maximum at 100 Myr, following an exponential functional form with a decay time of <span class="math notranslate nohighlight">\(10^7\)</span> yr. During this time, a total mass of <span class="math notranslate nohighlight">\(10^5\)</span> <span class="math notranslate nohighlight">\(M_\odot\)</span> of stars is formed.</p>
<p>Note that, for the IMF, CMF, and CLF, the absolute values of the weights to not matter, only their relative values. On the other hand, for the SFH, the absolute weight does matter.</p>
</section>
<section id="sampling-methods">
<span id="sampling-metod-label"></span><h2>Sampling Methods<a class="headerlink" href="#sampling-methods" title="Permalink to this headline">¶</a></h2>
<p>A final option allowed in both basic and advanced mode is a specification of the sampling method. The sampling method is a description of how to draw a population of objects from the PDF, when the population is specified as having a total sum <span class="math notranslate nohighlight">\(M_{\rm target}\)</span> (usually but not necessarily a total mass) rather than a total number of members <span class="math notranslate nohighlight">\(N\)</span>; there are a number of ways to do this, which do not necessarily yield identical distributions, even for the same underlying PDF. To specify a sampling method, simply add the line:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>method METHOD
</pre></div>
</div>
<p>to the PDF file. This line can appear anywhere except inside a <code class="docutils literal notranslate"><span class="pre">segment</span></code> specification, or before the <code class="docutils literal notranslate"><span class="pre">breakpoints</span></code> or <code class="docutils literal notranslate"><span class="pre">advanced</span></code> line that begins the file. The following values are allowed for <code class="docutils literal notranslate"><span class="pre">METHOD</span></code> (case-insensitive, as always):</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">stop_nearest</span></code>: this is the default option: draw until the total mass of the population exceeds <span class="math notranslate nohighlight">\(M_{\rm target}\)</span>. Either keep or exclude the final star drawn depending on which choice brings the total mass closer to the target value.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">stop_before</span></code>: same as <code class="docutils literal notranslate"><span class="pre">stop_nearest</span></code>, but the final object drawn is always excluded.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">stop_after</span></code>: same as <code class="docutils literal notranslate"><span class="pre">stop_nearest</span></code>, but the final object drawn is always kept.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">stop_50</span></code>: same as <code class="docutils literal notranslate"><span class="pre">stop_nearest</span></code>, but keep or exclude the final object with 50% probability regardless of which choice gets closer to the target.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">number</span></code>: draw exactly <span class="math notranslate nohighlight">\(N = M_{\rm target}/\langle M\rangle\)</span> object, where <span class="math notranslate nohighlight">\(\langle M\rangle\)</span> is the expectation value for a single draw.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">poisson</span></code>: draw exactly <span class="math notranslate nohighlight">\(N\)</span> objects, where the value of <span class="math notranslate nohighlight">\(N\)</span> is chosen from a Poisson distribution with expectation value <span class="math notranslate nohighlight">\(\langle N \rangle = M_{\rm target}/\langle M\rangle\)</span></p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">sorted_sampling</span></code>: this method was introduced by <a class="reference external" href="http://adsabs.harvard.edu/abs/2006MNRAS.365.1333W">Weidner &amp; Kroupa (2006, MNRAS. 365, 1333)</a>, and proceeds in steps. One first draws exactly <span class="math notranslate nohighlight">\(N= M_{\rm target}/\langle M\rangle\)</span> as in the <code class="docutils literal notranslate"><span class="pre">number</span></code> method. If the resulting total mass <span class="math notranslate nohighlight">\(M_{\rm pop}\)</span> is less than <span class="math notranslate nohighlight">\(M_{\rm target}\)</span>, the procedure is repeated recursively using a target mass <span class="math notranslate nohighlight">\(M_{\rm target} - M_{\rm pop}\)</span> until <span class="math notranslate nohighlight">\(M_{\rm pop} &gt; M_{\rm target}\)</span>. Finally, one sorts the resulting stellar list from least to most massive, and then keeps or removes the final, most massive star using a <code class="docutils literal notranslate"><span class="pre">stop_nearest</span></code> policy.</p></li>
</ul>
<p>See the file <code class="docutils literal notranslate"><span class="pre">lib/imf/wk06.imf</span></code> for an example of a PDF file with a <code class="docutils literal notranslate"><span class="pre">method</span></code> specification.</p>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">slug</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="getting.html">Getting SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="intro.html">Introduction to SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="compiling.html">Compiling and Installing SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="running.html">Running a SLUG simulation</a></li>
<li class="toctree-l1"><a class="reference internal" href="parameters.html">Parameter Specification</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Probability Distribution Functions</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#basic-mode">Basic Mode</a></li>
<li class="toctree-l2"><a class="reference internal" href="#variable-mode">Variable Mode</a></li>
<li class="toctree-l2"><a class="reference internal" href="#advanced-mode">Advanced Mode</a></li>
<li class="toctree-l2"><a class="reference internal" href="#sampling-methods">Sampling Methods</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="output.html">Output Files and Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="filters.html">Filters and Filter Data</a></li>
<li class="toctree-l1"><a class="reference internal" href="slugpy.html">slugpy – The Python Helper Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="cloudy.html">cloudy_slug: An Automated Interface to cloudy</a></li>
<li class="toctree-l1"><a class="reference internal" href="bayesphot.html">bayesphot: Bayesian Inference for Stochastic Stellar Populations</a></li>
<li class="toctree-l1"><a class="reference internal" href="cluster_slug.html">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
<li class="toctree-l1"><a class="reference internal" href="sfr_slug.html">sfr_slug: Bayesian Inference of Star Formation Rates</a></li>
<li class="toctree-l1"><a class="reference internal" href="tests.html">Test Problems</a></li>
<li class="toctree-l1"><a class="reference internal" href="library.html">Using SLUG as a Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="acknowledgements.html">Contributors and Acknowledgements</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="parameters.html" title="previous chapter">Parameter Specification</a></li>
      <li>Next: <a href="output.html" title="next chapter">Output Files and Format</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 4.1.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/pdfs.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>