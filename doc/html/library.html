
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Using SLUG as a Library &#8212; slug 2.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Contributors and Acknowledgements" href="acknowledgements.html" />
    <link rel="prev" title="Test Problems" href="tests.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="using-slug-as-a-library">
<span id="sec-library-mode"></span><h1>Using SLUG as a Library<a class="headerlink" href="#using-slug-as-a-library" title="Permalink to this headline">¶</a></h1>
<p>In addition to running as a standalone program, SLUG can be
compiled as a library that can be called by external programs. This is
useful for including stellar population synthesis calculations within
some larger code, e.g., a galaxy simulation code in which star
particles represent individual star clusters, where the stars in them
are treated stochastically.</p>
<section id="compiling-in-library-mode">
<span id="ssec-library-mode"></span><h2>Compiling in Library Mode<a class="headerlink" href="#compiling-in-library-mode" title="Permalink to this headline">¶</a></h2>
<p>To compile in library mode, simply do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make lib
</pre></div>
</div>
<p>in the main directory. This will cause a dynamically linked library
file <code class="docutils literal notranslate"><span class="pre">libslug.x</span></code> to be created in the <code class="docutils literal notranslate"><span class="pre">src</span></code> directory, where <code class="docutils literal notranslate"><span class="pre">x</span></code>
is whatever the standard extension for dynamically linked libraries on
your system is (<code class="docutils literal notranslate"><span class="pre">.so</span></code> for unix-like systems, <code class="docutils literal notranslate"><span class="pre">.dylib</span></code> for MacOS).</p>
<p>Alternately, if you prefer a statically-linked version, you can do:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make libstatic
</pre></div>
</div>
<p>and a statically-linked archive <code class="docutils literal notranslate"><span class="pre">libslug.y</span></code> will be created instead,
where <code class="docutils literal notranslate"><span class="pre">y</span></code> is the standard statically-linked library extension on
your system (generally <code class="docutils literal notranslate"><span class="pre">.a</span></code>).</p>
<p>In addition to <code class="docutils literal notranslate"><span class="pre">lib</span></code> and <code class="docutils literal notranslate"><span class="pre">libstatic</span></code>, the makefile supports
<code class="docutils literal notranslate"><span class="pre">lib-debug</span></code> and <code class="docutils literal notranslate"><span class="pre">libstatic-debug</span></code> as targets as well. These
compile the same libraries, but with optimization disabled and
debugging symbols enabled.</p>
<p>Finally, if you want MPI functionality, you can compile with:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>make lib MPI=ENABLE_MPI
</pre></div>
</div>
<p>See <a class="reference internal" href="compiling.html#ssec-compiling"><span class="std std-ref">Compiling</span></a> for more on compiling with MPI enabled.</p>
</section>
<section id="predefined-objects">
<span id="ssec-predefined-objects"></span><h2>Predefined Objects<a class="headerlink" href="#predefined-objects" title="Permalink to this headline">¶</a></h2>
<p>In order to make it more convenient to use slug as a library, the
library pre-defines some of the most commonly-used classes, in order
to save users the need to construct them. These predefined objects can
be accessed by including the file <code class="docutils literal notranslate"><span class="pre">slug_predefined.H</span></code> in your source
file. This function defines the class <code class="docutils literal notranslate"><span class="pre">slug_predef</span></code>, which
pre-defines all the IMFs, evolutionary tracks, spectral synthesizers,
and yields that ship with slug, without forcing the user to interact
with the parameter parsing structure.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">slug_predef</span></code> class provides the methods <code class="docutils literal notranslate"><span class="pre">imf</span></code>, <code class="docutils literal notranslate"><span class="pre">tracks</span></code>,
<code class="docutils literal notranslate"><span class="pre">specsyn</span></code>, and <code class="docutils literal notranslate"><span class="pre">yields</span></code>. These methods take as arguments a string
specifying one of the predefined names of an IMF, set of tracks, or
spectral synthesizer, and return an object of that class that can then
be passed to <code class="docutils literal notranslate"><span class="pre">slug_cluster</span></code> to produce a cluster object. For
example, the following sytax creates a <code class="docutils literal notranslate"><span class="pre">slug_cluster</span></code> with ID number
1, a mass of 100 solar masses, age 0, a Chabrier IMF, Padova solar
metallicity tracks, starburst99-style spectral synthesis, and slug’s
default nuclear yields:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>#include &quot;slug_predefined.H&quot;
#include &quot;slug_cluster.H&quot;

slug_cluster *cluster =
   new slug_cluster(1, 100.0, 0.0, slug_predef.imf(&quot;chabrier&quot;),
                    slug_predef.tracks(&quot;modp020.dat&quot;),
                    slug_predef.specsyn(&quot;sb99&quot;),
                    nullptr, nullptr, nullptr,
                    slug_predef.yields());
</pre></div>
</div>
</section>
<section id="using-slug-as-a-library-with-mpi-enabled-codes">
<span id="ssec-mpi-support"></span><h2>Using SLUG as a Library with MPI-Enabled Codes<a class="headerlink" href="#using-slug-as-a-library-with-mpi-enabled-codes" title="Permalink to this headline">¶</a></h2>
<p>In large codes where one might wish to use slug for subgrid stellar
models, it is often necessary to pass information between processors
using MPI. Since slug’s representation of stellar populations is
complex, and much information is shared between particles rather than
specific to individual particles (e.g., tables of yields and
evolutionary tracks), passing slug information between processors is
non-trivial.</p>
<p>To facilitate parallel implementations, slug provides routines that
wrap the base MPI routines and allow seamless and efficient exchange
of the slug_cluster class (which slug uses to represent simple stellar
populations) between processors. The prototypes for these functions
are found in the <code class="docutils literal notranslate"><span class="pre">src/slug_MPI.H</span></code> header file, and the functions are
available if the library was compiled with MPI support enabled (see
<a class="reference internal" href="#ssec-library-mode"><span class="std std-ref">Compiling in Library Mode</span></a>).</p>
<p>Here is an example of MPI usage, in which one processor creates a
cluster and then sends it to another one:</p>
<div class="highlight-rest notranslate"><div class="highlight"><pre><span></span>#include &quot;slug_cluster.H&quot;
#include &quot;slug_MPI.H&quot;
#include &quot;mpi.h&quot;
#include &lt;vector&gt;
#include &lt;cstdio&gt;

int main(int argc, char *argv[]) {

  // Start MPI
  MPI_Init(&amp;argc, &amp;argv);

  // Get rank
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &amp;rank);

  // Rank 0 creates a cluster and prints out the masses of the stars
  slug_cluster *cluster;
  if (rank == 0) {
    cluster =
       new slug_cluster(1, 100.0, 0.0, slug_predef.imf(&quot;chabrier&quot;),
                        slug_predef.tracks(&quot;modp020.dat&quot;),
                        slug_predef.specsyn(&quot;sb99&quot;),
                        nullptr, nullptr, nullptr,
                        slug_predef.yields());
    const std::vector&lt;double&gt; stars = cluster-&gt;get_stars();
    for (int j=0; j&lt;stars.size(); j++)
      std::cout &lt;&lt; &quot;rank 0, star &quot; &lt;&lt; j
                &lt;&lt; &quot;: &quot; &lt;&lt; stars[j] &lt;&lt; std::endl;
  }

  // Barrier to make sure rank 0 outputs come first
  MPI_Barrier(MPI_COMM_WORLD);

  // Rank 0 sends cluster, rank 1 receives it
  if (rank == 0) {
    MPI_send_slug_cluster(*cluster, 1, 0, MPI_COMM_WORLD);
  } else if (rank == 1) {
    cluster = MPI_recv_slug_cluster(0, 1, MPI_COMM_WORLD,
                                    slug_predef.imf(&quot;chabrier&quot;),
                                    slug_predef.tracks(&quot;modp020.dat&quot;),
                                    slug_predef.specsyn(&quot;sb99&quot;),
                                    nullptr, nullptr, nullptr,
                                    slug_predef.yields());
  }

  // Rank 1 prints the masses of the stars; the resulting masses
  // should be identical to that produced on rank 0
  if (rank == 1) {
    const std::vector&lt;double&gt; stars = cluster-&gt;get_stars();
    for (int j=0; j&lt;stars.size(); j++)
      std::cout &lt;&lt; &quot;rank 1, star &quot; &lt;&lt; j
                &lt;&lt; &quot;: &quot; &lt;&lt; stars[j] &lt;&lt; std::endl;
  }
}
</pre></div>
</div>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">slug</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="getting.html">Getting SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="intro.html">Introduction to SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="compiling.html">Compiling and Installing SLUG</a></li>
<li class="toctree-l1"><a class="reference internal" href="running.html">Running a SLUG simulation</a></li>
<li class="toctree-l1"><a class="reference internal" href="parameters.html">Parameter Specification</a></li>
<li class="toctree-l1"><a class="reference internal" href="pdfs.html">Probability Distribution Functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="output.html">Output Files and Format</a></li>
<li class="toctree-l1"><a class="reference internal" href="filters.html">Filters and Filter Data</a></li>
<li class="toctree-l1"><a class="reference internal" href="slugpy.html">slugpy – The Python Helper Library</a></li>
<li class="toctree-l1"><a class="reference internal" href="cloudy.html">cloudy_slug: An Automated Interface to cloudy</a></li>
<li class="toctree-l1"><a class="reference internal" href="bayesphot.html">bayesphot: Bayesian Inference for Stochastic Stellar Populations</a></li>
<li class="toctree-l1"><a class="reference internal" href="cluster_slug.html">cluster_slug: Bayesian Inference of Star Cluster Properties</a></li>
<li class="toctree-l1"><a class="reference internal" href="sfr_slug.html">sfr_slug: Bayesian Inference of Star Formation Rates</a></li>
<li class="toctree-l1"><a class="reference internal" href="tests.html">Test Problems</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Using SLUG as a Library</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#compiling-in-library-mode">Compiling in Library Mode</a></li>
<li class="toctree-l2"><a class="reference internal" href="#predefined-objects">Predefined Objects</a></li>
<li class="toctree-l2"><a class="reference internal" href="#using-slug-as-a-library-with-mpi-enabled-codes">Using SLUG as a Library with MPI-Enabled Codes</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="acknowledgements.html">Contributors and Acknowledgements</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="tests.html" title="previous chapter">Test Problems</a></li>
      <li>Next: <a href="acknowledgements.html" title="next chapter">Contributors and Acknowledgements</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2014, Mark Krumholz, Michele Fumagalli, et al..
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 4.1.1</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/library.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>